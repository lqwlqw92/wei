<?php

namespace  wei\core;
/**
 * 路由基类
 *
 * Class Route
 */
class Route
{
    public $group;
    public $controller;
    public $action;
    public $params;
    
    public function __construct()
    {
    
    }
    
    public function init()
    {
        $route = $this->getRequest();
        $this->group      = $route['group'];
        $this->controller = $route['controller'];
        $this->action     = $route['action'];
        !empty($route['param']) && $this->params = $route['param'];
    }
    
    /*
     * 支持pathinfo形式
     */
    public function getRequest()
    {
        $filter_param = array('<','>','"',"'",'% 3C','% 3E','% 22','% 27','% 3c','% 3e');
        $uri  = str_replace($filter_param,'',$_SERVER['REQUEST_URI']);
        $path = parse_url($uri);
        if (strpos($path['path'],'index.php') == 0)
        {
            //处理urlwrite后的url 此时不带index.php
            $urlR0 = $path['path'];
        }
        else
        {
            $urlR0 = substr($path['path'],strlen('index.php')+1);
        }
        
        $urlR = ltrim($urlR0,'/');
    
        if ($urlR === '') {
            $route = $this->parseTradition();
            return $route;
        }
        
        $req_arr = explode('/',$urlR);
        //去除空白
        foreach($req_arr as $key=>$value)
        {
            if (empty($value))
            {
                unset($req_arr[$key]);
            }
        }
        $cnt = count($req_arr);
        if (empty($req_arr) || empty($req_arr[0]))
        {
            $cnt = 0;
        }
    
        switch($cnt)
        {
            case 0:
                $route['controller'] = $GLOBALS['_config']['defaultController'];
                $route['action']     = $GLOBALS['_config']['defaultAction'];
                $route['group']      = $GLOBALS['_config']['defaultApp'];
                break;
            case 1:
                if (stripos($req_arr[0],':'))
                {
                    $gc = explode(':',$req_arr[0]);
                    $route['group']       = $gc[0];
                    $route['controller']  = $gc[1];
                    $route['action']      = $GLOBALS['_config']['defaultAction'];
                }
                else
                {
                    $route['controller'] = $req_arr[0];
                    $route['action']     = $GLOBALS['_config']['defaultAction'];
                    $route['group']      = $GLOBALS['_config']['defaultApp'];
                }
                break;
            default:
                if (stripos($req_arr[0],':'))
                {
                    $gc = explode(':',$req_arr[0]);
                    $route['group']     = $gc[0];
                    $route['controller']  = $gc[1];
                    $route['action']    = $req_arr[1];
                }
                else
                {
                    $route['controller'] = $req_arr[0];
                    $route['action']     = $req_arr[1];
                    $route['group']      = $GLOBALS['_config']['defaultApp'];
                }
                
                for($i=2;$i<$cnt;$i++)
                {
                    $route['param'][$route[$i]] = isset($req_arr[++$i]) ? $req_arr[$i]:'';
                }
                break;
        }
        
   
        if (!empty($path['query']))
        {
            parse_url($path['query']);
            if (empty($route['param']))
            {
                $route['param'] = [];
            }
            
            $route['param'] += $route;
        }
        
        return $route;
        
        
    }
    
    /**
     * 获取配置默认值
     *
     * @return array
     */
    public function parseTradition()
    {
        $route = [];
        if (!isset($_GET[$GLOBALS['_config']['UrlGroupName']]))
        {
            $_GET[$GLOBALS['_config']['UrlGroupName']] = '';
        }
        if (!isset($_GET[$GLOBALS['_config']['UrlControllerName']]))
        {
            $_GET[$GLOBALS['_config']['UrlControllerName']] = '';
        }
        if (!isset($_GET[$GLOBALS['_config']['UrlActionName']]))
        {
            $_GET[$GLOBALS['_config']['UrlActionName']] = '';
        }
        
        $route['group'] = $_GET[$GLOBALS['_config']['UrlGroupName']];
        $route['controller'] = $_GET[$GLOBALS['_config']['UrlControllerName']];
        $route['action'] = $_GET[$GLOBALS['_config']['UrlActionName']];
        
        unset($_GET[$GLOBALS['_config']['UrlGroupName']]);
        unset($_GET[$GLOBALS['_config']['UrlControllerName']]);
        unset($_GET[$GLOBALS['_config']['UrlActionName']]);
        
        $route['param'] = $_GET;
        if ($route['group'] == NULL)
        {
            $route['group'] =  $GLOBALS['_config']['defaultApp'];
        }
        if ($route['controller'] == NULL)
        {
            $route['controller'] =  $GLOBALS['_config']['defaultController'];
        }
        if ($route['action'] == NULL)
        {
            $route['action'] =  $GLOBALS['_config']['defaultAction'];
        }
        
        return $route;
    }
}