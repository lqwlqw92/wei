<?php
/**
 * 请求基类
 * User: liuqingwei
 * Date: 2020/4/1
 * Time: 下午11:42
 */

namespace wei\core;

class Request
{
    /**
     * 获取指定参数的值
     *
     * @param string $param 参数名称
     *
     * @return mixed
     */
    public function getParam($param)
    {
        if (isset($_REQUEST[$param]))
        {
            return $_REQUEST[$param];
        }
        else
        {
            return NULL;
        }
    }
    
    /**
     * get请求
     *
     * @param string $param 参数名称
     *
     * @return mixed
     */
    public function get($param)
    {
        return $this->getParam($param);
    }
    
    /**
     * post请求
     *
     * @param string $param 参数名称
     *
     * @return mixed
     */
    public function post($param)
    {
        return $this->getParam($param);
    }
}