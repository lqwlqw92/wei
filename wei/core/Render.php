<?php
/**
 * 模版引擎接口类
 * User: liuqingwei
 * Date: 2020/3/29
 * Time: 上午11:59
 */

namespace  wei\core;

interface Render
{
    public function init();
    public function assign($key,$value);
    public function display($view='');
}