<?php
/**
 * 简单的orm model 基类
 * User: liuqingwei
 * Date: 2020/3/28
 * Time: 下午11:23
 */

namespace  wei\core;


class Model
{
    public function save()
    {
        $reflect = new \ReflectionClass($this);
        $pros = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        $sql_template = "INSERT INTO ".$this->getTableNameByPO($reflect)."(";
        $key_array = array_column($pros,'name');
        $keys = implode(',',$key_array);
        $prepare_keys = implode(',',array_map(function($key){return ':'.$key;},$key_array));
        $sql_template = "INSERT INTO ".$this->getTableNameByPO($reflect)."({$keys}) VALUES ($prepare_keys)";
        $data = [];
        foreach($pros as $v)
        {
            $data[$v->name] = $reflect->getProperty($v->name)->getValue($this);
        }
    
        $db = DB::getInstance($GLOBALS['_config']['db']);
        
        $ret = $db->execute($sql_template,$data);
        return $ret;
    }
    
    public function deleteByPid()
    {
    
    }
    
    public function update()
    {
    
    }
    
    public function find()
    {
    
    }
    
    public function buildPrimaryWhere()
    {
    
    }
    
    /**
     * 获取完整的表名称
     *
     * @param string     $tableName  去除前缀的表名
     * @param string     $prefix     表前缀
     *
     * @return string
     */
    public function getRealTableName($tableName,$prefix='')
    {
        if (!empty($prefix))
        {
            $realTableName = $prefix."_{$tableName}";
        }
        elseif(isset($GLOBALS['_config']['db']['prefix']) && !empty($GLOBALS['_config']['db']['prefix']))
        {
            $realTableName = $GLOBALS['_config']['db']['prefix']."_{$tableName}";
        }
        else
        {
            $realTableName = $tableName;
        }
        return $realTableName;
    }
    
    /**
     * 获取完整的表名称
     *
     * @param string     $tableName  去除前缀的表名
     * @param string     $prefix     表前缀
     *
     * @return string
     */
    public function buildPO($tableName,$prefix)
    {
        $db = DB::getInstance($GLOBALS['_config']['db']);
        $ret = $db->query('SELECT * FROM `information_schema`.`COLUMNS` WHERE TABLE_NAME = : TABLENAME',[
            'TABLENAME' => $this->getRealTableName($tableName,$prefix),
        ]);
        $class_name = '';
        foreach(explode('_',ucwords($tableName)) as $name)
        {
            $class_name .= $name;
        }
        
        $file = _APP.'model/'.$class_name.'.php';
        
        $class_string = "<?php \r\nclass $class_name extend Model { \r\n";
        foreach($ret as $key=>$value)
        {
            $class_string = 'public $'."{$value['COLUMN_NAME']};";
            if (!empty($value['COLUMN_NAME']))
            {
                $class_string .= "                  // {$value['COLUMN_NAME']}";
            }
            
            $class_string .= "\r\n";
        }
        $class_string = "}";
        file_put_contents($file,$class_string);
    }
    
    /**
     * 根据类名生成表名
     *
     * @param object $reflect 发射对象名称
     *
     * @return string
     */
    public function getTableNameByPO($reflect)
    {
        $table_name = $reflect->getShortName();
        $table_name = preg_split("/(?=[A-Z])/",$table_name);
        array_shift($table_name);
        $table_name = implode("_",array_map("strtolower",$table_name));
        return $this->getRealTableName($table_name);
    }
    
}