<?php
/**
 * 拦截器接口类
 * User: liuqingwei
 * Date: 2020/4/1
 * Time: 下午11:28
 */

namespace wei\core;

interface InterceptorInterface
{
    /**
     * 前置拦截器
     */
    public function preHandle();
    
    /**
     * 后置拦截器
     */
    public function postHandle();
}