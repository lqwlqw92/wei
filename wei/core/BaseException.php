<?php
/**
 * 异常处理基类
 * User: liuqingwei
 * Date: 2020/4/1
 * Time: 下午10:08
 */

namespace wei\core;

class BaseException extends \Exception
{
    public function __toString()
    {
        return (new self())->getMessage();
    }
    
    /**
     * 日志
     */
    public static function _log()
    {
        $err  = date('Y-m-d H:i:s')."|";
        $err .= '错误信息'.(new self())->getMessage().'|';
        $err .= '错误code'.(new self())->getCode().'|';
        $err .= '堆栈回溯信息'.json_encode(debug_backtrace()).PHP_EOL;
        file_put_contents(_APP.'log.txt',$err,FILE_APPEND);
    }
    
    public function errorMessage()
    {
        self::_log();
    }
}