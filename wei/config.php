<?php

return [
    'mode'          => 'debug', //默认为调试模式
    'filter'        => true,    //是否过滤
    'charSet'       => 'utf-8', //编码格式
    'defaultApp'    => 'front', //默认应用
    'defaultController' => 'index', //默认的控制器名称
    'defaultAction'     => 'index', //默认的动作名称
    'UrlControllerName' => 'c',     //自定义的控制器名称
    'UrlActionName'     => 'a',     //自定义的方法名称
    'UrlGroupName'      => 'g',     //自定义的分组名称
    'db'                => [
        'dsn'       => 'mysql:host=127.0.0.1;port=3306;dbname=easyswoole',
        'username'  => 'root',
        'passward'  => '521lwj',
        'param'     => '',
        'prefix'    => 'easy',
    ],
    'smtp'              => [],
    
    'interceptor' => [
        'app\front\module\interceptor\LoginInterceptor' => '*',
    ],
];