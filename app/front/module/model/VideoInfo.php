<?php
/**
 * Created by PhpStorm.
 * User: liuqingwei
 * Date: 2020/3/29
 * Time: 上午12:37
 */

namespace app\front\module\model;

use Illuminate\Database\Eloquent\Model;

class VideoInfo extends Model
{
   protected $table = 'video_info';
   public $timestamps = false;
}