<?php

namespace app\front\module\controller;

use app\front\module\model\VideoInfo;
use library\container\Container;
use wei\core\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $video = new VideoInfo();
        $video->id = 24;
        $video->age = 20;
        $video->name = 'en';
        $video->save();
        $this->assign('age',$video->age+1);
        $this->display();
    }
    
    public function hiAction()
    {
        $video = new VideoInfo();
        $video->id = 590;
        $video->age = 20;
        $video->name = 'en';
        $containar = new Container();
        $containar->set('video',$video);
        $video_key = $containar->video;
        $containar->inject("video",'method',function()use($video_key){
            $video_key->save();
            for($i=0;$i<10;$i++)
            {
                echo $i;
            }
        });
        $containar->callback('video','method');
        echo "hiAction";
    }
    
    public function updateAction()
    {
    
    }
    
    
}