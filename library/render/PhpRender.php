<?php
/**
 * 模版引擎类
 * User: liuqingwei
 * Date: 2020/3/29
 * Time: 下午12:03
 */

namespace library\render;


use wei\core\Render;

class PhpRender implements Render
{
    private $value = array();
    
    public function init()
    {
    
    }
    
    
    /**
     * 视图展示
     *
     * @param string $view html代码
     *
     * @return mixed
     */
    public function display($view = '')
    {
        extract($this->value,NULL);
        include $view;
    }
    
    /**
     * 变量赋值到模版引擎
     *
     * @param string        $key
     * @param string|array $value
     *
     * @return mixed
     */
    public function assign($key, $value )
    {
        $this->value[$key] = $value;
    }
}