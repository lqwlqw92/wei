<?php
/**
 * 容器接口
 * User: liuqingwei
 * Date: 2020/4/2
 * Time: 下午4:57
 */

namespace library\container;

interface ContainerInterface
{
    public function get($key);
    public function set($key,$value);
    public function has($key);
}