<?php
/**
 * 容器基类
 * User: liuqingwei
 * Date: 2020/4/2
 * Time: 下午4:59
 */

namespace library\container;

class ContainerAccess implements \ArrayAccess
{
    //容器池
    private $keys = [];
    
    public function __construct(array $value=[])
    {
    
    }
    
    /**
     * 判断类是否在池中
     *
     * @param mixed $offset key值
     *
     * @return bool
     */
    public function offsetExists( $offset )
    {
        return isset($this->keys[$offset]);
    }
    
    /**
     * 获取容器
     *
     * @param mixed $offset key值
     *
     * @return mixed
     */
    public function offsetGet( $offset )
    {
        return $this->keys[$offset];
    }
    
    /**
     * 注入容器池中
     *
     * @param mixed $offset key
     * @param mixed $value  类
     *
     * @return mixed
     */
    public function offsetSet( $offset, $value )
    {
        $this->keys[$offset] = $value;
    }
    
    /**
     * 删除容器的某个key
     *
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetUnset( $offset )
    {
        if (isset($this->keys[$offset]))
        {
            unset($this->keys[$offset]);
        }
    }
}