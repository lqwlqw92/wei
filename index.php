<?php

define("_ROOT",__DIR__.DIRECTORY_SEPARATOR);        //网站根目录
define("_SYS_PATH",_ROOT.'wei'.DIRECTORY_SEPARATOR);//系统目录
define("_APP",_ROOT.'app'.DIRECTORY_SEPARATOR);     //应用根目录
define("_VENSION",'0.1');                           //版本记录
$GLOBALS['_config'] = require _SYS_PATH.'config.php'; //配置文件
require _ROOT."vendor/autoload.php";
require _SYS_PATH."wei.php";

if ('debug' == $GLOBALS['_config']['mode'])
{
    if (substr(PHP_VERSION,0,3)>= "5.5")
    {
        error_reporting(E_ALL);
    }
    else
    {
        error_reporting(E_ALL | E_STRICT);
    }
}
else
{
    set_error_handler(["wei",'errorHandler']);
}
set_exception_handler(["wei","exceptionHandler"]);

$app = new wei;
$app->run();